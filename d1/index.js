console.log('Hello World!');

//Parameters and Arguments
		
/*
	Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	
	Functions are mostly created to create complicated tasks to run several lines of code in succession
	
	They are also used to prevent repeating lines/blocks of codes that perform the same task/function

	We also learned in the previous session that we can gather data from user input using a prompt() window.

*/

/*


function printInput(){
    let nickName = prompt('Enter your nickname');
    console.log('Hi, ' + nickName);
}


// We could also invoke/call a function in our browser's console
printInput();


*/

/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

	"name" is called a parameter
	A "parameter" acts as a named variable/container that exists only inside of a function
	It is used to store information that is provided to a function when it is called/invoked.
    "Juana", the information/data provided directly into the function is called an argument.
	Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
*/


// Function with parameters and arguments
function printName(name){ // "name" is a Parameter
    console.log('My name is ' + name)
}

printName('Juana'); // Argument
printName('John');

// When the "printName()" function is first called, it stores the value of "John" in the parameter "name" then uses it to print a message.

// When the "printName()" function is called again, it stores the value of "Jane" in the parameter "name" then uses it to print a message.
printName("Jane");

// Variables can also be passed as an argument
let sampleVariable = 'Yui';
printName(sampleVariable);

// Trying invocation with number as an argument
// printName(404);

//Function arguments cannot be used by a function if there are no parameters provided within the function.
function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log('The remainder of ' + num + ' divided by 8 is: ' + remainder);

    let isDivisibileBy8 = remainder === 0;
    console.log('Is ' + num + 'divisible by 8?');
    console.log(isDivisibileBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

console.log('------------------------');
// Function as argument
    // Function parameters can also accept other functions as arguments.
    // Some complex functions uses other functions as arguments to perform more complicated results.
function argumentFunction(){
    console.log('This function was passed as an argument before the message was printed');
}

function invokeFunction(argumentFunctionInParameter){
    argumentFunction();
}


// A function used without a parenthesis is normally associated with using the function as an argument to another function.
invokeFunction(argumentFunction);
console.log(argumentFunction);

// Using multiple parameters

    // Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order

function createFullName(firstName, middleName, lastName){
    console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName('Juan','Dela', 'Cruz')
/*
		"Juan" will be stored in the parameter "firstName"
		"Dela" will be stored in the parameter "middleName"
		"Cruz" will be stored in the parameter "lastName"
		
		----------------------

		In JavaScript, providing more/less arguments than the expected parameters will not return an error.

		Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

		In other programming languages, this will return an error stating that "the expected number of arguments do not match the number of parameters".
	*/

createFullName('Juan', 'Dela');
createFullName('Juan', 'Dela', 'Cruz', 'Dalisay');

// Using variables as argument
let firstName = 'Cardo';
let middleName = 'Sta. Maria';
let lasttName = 'Dalisay';

createFullName(firstName, middleName, lasttName);
/*
		Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

		The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.
	*/

function printFullName(middleName, firstName, lastName){
    console.log(firstName + ' ' + middleName + ' ' + lastName);
}

printFullName('Christopher', 'Katigbak', 'Malinao');
// results to "Katigbak Christopher Malinao" because "Christopher" was received as middleName, "Katigbak" was received as firstName.


// The return statement
    // The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){
	// console.log("Now you see me");
    return firstName + ' ' + middleName + ' ' + lastName
	// console.log("You can't see me");
}

let completeName = returnFullName('Jeffrey', 'Smith', 'Bezos');
console.log(completeName);
console.log(returnFullName('Jeffrey', 'Smith', 'Bezos'));
// console.log(returnFullName(fName, mName, lName)); // error
// error because of no initialization for fName, mName, lName

// function checkIfLegalAge(age){
// 	return age >= 18; //true
// }

// let isLegalAge = checkIfLegalAge(22);
// console.log(isLegalAge);


// Creating a variable inside a function
function returnAddress(city, country){
	let fullAddress = city + ', ' + country;
	return fullAddress;
}

let myAddress = returnAddress('Cebu City', 'Philippines');
console.log(myAddress);

// A teacher always gives a chocomucho/cloud 9 for his students who have grades equal or over 90

function checkGradeIfWithPrize(score, total){
	let scoreGrade = ((score/total)*100) >= 90
	return scoreGrade;
}

let withPrize = checkGradeIfWithPrize(38, 50)
console.log('With Prize?: ' + withPrize);


// Without return statement
	// On the other hand, when a function the only has console.log() to display its result it will return undefined instead.

function printPlayerInfo(username, level, job){
	console.log('Username: ' + username);
	console.log('Level: ' + level);
	console.log('Job: ' + job);
	return username + ' | ' + level + ' | ' + job
}

let user1 = printPlayerInfo('bhoxzMapagmahal', 95, 'NPC');
console.log(user1);

//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

//You cannot save any value from printPlayerInfo() because it does not return anything.